<?php

function arphabet_widgets_init() {

	register_sidebar( array(
		'name' => 'sidebar',
		'id' => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

add_theme_support('post-thumbnails');

add_theme_support( 'title-tag' );

add_theme_support(
  'html5',
  array(
    'search-form',
    'gallery',
    'caption',
    'script',
    'style',
    'navigation-widgets',
  )
);


add_theme_support( 'align-wide' );

add_theme_support( 'responsive-embeds' );

add_action( 'init', 'create_post_types' );
function create_post_types() {
  register_post_type( 'chants',
    array(
      'labels' => array(
        'name' => __( 'Chants' )
      ),
      'public' => true,
      'supports' => array('editor','title','custom-fields'),
	  'menu_position' => 5
    )
  );

  register_post_type( 'icons',
    array(
      'labels' => array(
        'name' => __( 'Icons' )
      ),
      'public' => true,
      'supports' => array('editor','title','custom-fields', 'thumbnail'),
	  'menu_position' => 6
    )
  );
}

function gongle_the_field($name) {
  global $post;
  echo get_post_meta($post->ID, $name, true);
}