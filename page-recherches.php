<?php get_header(); ?>

<div class="background_image">
	<?php the_post_thumbnail( 'full' )?>
</div>

<div class="page_wrapper recherches container-fluid g-lg-0">

	<div class="connector_div">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/connector04.png">
	</div>

	<section id="nav" class="sommaire_list page_post">
		<div class="row">
			<div class="offset-lg-2 col-lg-6">
				<h1>Recherches</h1>
			</div>
		</div>
		<ul>
			<?php
				global $post;
				$args = array( 'posts_per_page' => 10, 'category' => 6 );
				$posts = get_posts( $args );
				foreach ($posts as $post) :
			?>
			<li class="row">
				<div class="offset-lg-1 col-lg-6">
					<a href="#post-<?php the_ID(); ?>" target="_self">
						<?php the_title(); ?>
					</a>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</section><!-- end of sommaire -->

	<div class="connector_div_2">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/connector_arrow3.png">
	</div>

	<?php query_posts("cat=6"); ?>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<section class="page_post row">
		<div class="post_title offset-lg-1 col-lg-3 order-lg-last">
			<span><?php the_title(); ?></span>
			<div class="top_link">
				<a href="#nav">retour au sommaire / <br><span class="english">back to index</span></a>
		</div>
		</div>
		<div class="post_content offset-lg-1 col-lg-6" id="post-<?php the_ID(); ?>">
			<?php the_content(); ?>
		</div>
	</section>

	<?php endwhile; else: ?>
	<?php endif; ?>

</div><!-- end of page_wrapper -->


<?php get_footer(); ?>