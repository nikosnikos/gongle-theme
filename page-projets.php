<?php get_header(); ?>

<div class="project_page_wrapper page_post">
	<div class="container">
			<?php
			$sticky = get_option('sticky_posts');
			if ($sticky):
				$args  = array(
					'post__in'            => $sticky,
					'ignore_sticky_posts' => true,
					'cat'                 => 3,
				);
				$query = new WP_Query($args);
				?>
				<?php if ($query->have_posts()): ?>
					<h1 class="offset-2">Créations au répertoire</h1>
					<div class="project_list mb-5">
							<?php while ($query->have_posts()):
								$query->the_post(); ?>
									<a href="<?php the_permalink() ?>" rel="bookmark" title="">
										<div class="row">
											<div class="project_list_name col-8 offset-lg-1">
												<span><?php the_title(); ?></span>
											</div>
											<div class="project_list_year col-4 col-lg-2">
												<span>
													<?php echo get_post_meta($post->ID, 'Year', true); ?>
												</span>
											</div>
										</div>
									</a>
							<?php endwhile; ?>
					</div>
				<?php endif; ?>
			<?php endif; // $sticky ?>
			<h1 class="offset-2">Projets</h1>
			<div class="project_list">
				<?php
				$args  = array(
					'post__not_in' => $sticky,
					'cat'          => 3,
				);
				$query = new WP_Query($args);
				?>
					<?php if ($query->have_posts()):
						while ($query->have_posts()):
							$query->the_post(); ?>
								<a href="<?php the_permalink() ?>" rel="bookmark" title="">
									<div class="row">
										<div class="project_list_name col-8 offset-lg-1 text-truncate">
											<span><?php the_title(); ?></span> <?php if (has_category(50)) { echo '<strong class="ms-2" style="font-size: 0.7em;">En création</strong>'; } elseif (in_category(51)) { echo '<strong class="ms-2" style="font-size: 0.7em;">En tournée</strong>'; } ?>
										</div>
										<div class="project_list_year col-4 col-lg-2">
											<span>
												<?php echo get_post_meta($post->ID, 'Year', true); ?>
											</span>
										</div>
									</div>
								</a>
						<?php endwhile;?>
					<?php endif; ?>
			</div>
	</div>

</div><!-- end of project_page_wrapper -->

<?php get_footer(); ?>
