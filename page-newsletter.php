<?php get_header(); ?>



<div class="row">

	<div id="newsletter_wrapper" class="offset-2 col-8">

		<div class="newsletter_pre">

			<p>S'inscrire à la newsletter</p>

			<p class="english">Sign up to the newsletter</p>

		</div>

		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/newsletter_arrow_down.png">

		<div id="newsletter_block">

			<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7_dtp.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{padding-top:  160px; background:#f8f8f8; clear:left; font:14px "Liberation Sans",Helvetica,Arial,sans-serif; width:100%;} #mc_embed_signup .button {background-color: #3c393e; font-family: "Liberation Sans";} #mc_embed_signup input.email {font-family: "Liberation Sans"; background-color: #f8f8f8;}
	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="https://gongle.us7.list-manage.com/subscribe/post?u=8ab9bdc5687ec9cf5ecb4cfea&amp;id=d4b94d19ec" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
			<div class="input-group mx-auto mt-3" style="width: 350px;">
				<input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="Adresse email" required aria-label="Adresse email pour s'inscrire à sendinblue" aria-describedby="button-addon">
				<div class="input-group-append">
					<input type="submit" value="S'inscrire" name="subscribe" id="button-addon"  class="btn btn-secondary">
				</div>
			</div>
			<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			<div style="position: absolute; left: -5000px;" aria-hidden="true">
				<input type="text" name="b_8ab9bdc5687ec9cf5ecb4cfea_d4b94d19ec" tabindex="-1" value="">
			</div>
    </div>
</form>
</div>

<div class="mt-5">
	<p>Contact : <u><span class="email_1">contact</span><span class="email_2">gongle</span>fr</u></p>
</div>

<!--End mc_embed_signup-->

		<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar')) : ?>

		<?php endif; ?>

		</div>

	</div>

</div>



<?php get_footer(); ?>