<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Chants des Négociations</title>
        <meta name="description" content="Chants des Négociations">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/chants-style.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.smooth-scroll.min.js"></script>
        <script type="text/javascript">
        	$(document).ready( function(){

			// MENU TOGGLE			
				
				// ARROW HOVER
				
				$(".menu_toggle").hover( function(){
					$(this).find(".no_hover").css("opacity", "0");
					$(this).find(".hover").css("opacity", "1");
				}, function(){
					$(this).find(".no_hover").css("opacity", "1");
					$(this).find(".hover").css("opacity", "0");
				});
								
				$(".menu_hide a").on("click", function(e){
					e.preventDefault();
					$("#menu, .menu_hide").css({
						"margin-right": "-40%",
						"-webkit-transition": "margin-right 1s",
						"transition": "margin-right 1s"						
					});					
				});
									
				$(".menu_show a").on("click", function(e){
					e.preventDefault();
					$("#menu, .menu_hide").css({
						"margin-right": "0%",
						"-webkit-transition": "margin-right 1s",
						"transition": "margin-right 1s"						
					});
				});									
			
			// LOGO HOVER	
				$("#logo").hover( function(){
					$(this).find(".no_hover").css("opacity", "0");
					$(this).find(".hover").css("opacity", "1");
				}, function(){
					$(this).find(".no_hover").css("opacity", "1");
					$(this).find(".hover").css("opacity", "0");
				});
			
			// SONG SELECTION	
				
				$(".play_button").hover( function(){
					$(this).find(".no_hover").css("opacity", "0");
					$(this).find(".hover").css("opacity", "1");
				}, function(){
					$(this).find(".no_hover").css("opacity", "1");
					$(this).find(".hover").css("opacity", "0");
				});
							
				// INITIALISE
				$("#songs li:first").addClass("current");
				$("#menu li:first").addClass("current");
				/*
				var initialise = function (){
					var rawId = $("#menu li:first").attr("id");
					var url = $("#song-" + rawId).find(".iframe").text();
					$.get( url, function( data ) {
						$( ".player_space" ).html( data );
					});					
				}
				initialise();
				*/
								
				var currentSong = $("#songs .current").find(".song_title").text();
				//$(".current_player").text(currentSong);

				$("#menu li a").on("click", function(e){
					e.preventDefault();
					var rawId = $(this).parent("li").attr("id");					
					// modify selected li
					$("#menu .current").removeClass("current");
					$("#songs .current").removeClass("current");
					$(this).parent("li").addClass("current");
					$("#song-" + rawId).addClass("current");
					$("body").animate({
						scrollTop: $("#song-" + rawId).offset().top -40
					}, 1000);									
				});
				
				$(".play_button").on("click", function(e){
					e.preventDefault();
					$(".play_button").show();
					$(this).hide();
					// get id
					var rawId = $(this).parents("li").attr("id");
					var url = $("#" + rawId).find(".iframe").text();
					// load player					
					$.get( url, function( data ) {						
						$( ".player_space" ).html( data );
					});
					// modify selected song
					$("#menu .current").removeClass("current");
					$("#songs .current").removeClass("current");
					$(this).parents("li").addClass("current");
					var menuId = rawId.substring(5);
					$("#" + menuId).addClass("current");
					// change song title in player
					var currentSong = $("#songs .current").find(".song_title").text();
					$(".current_player").text(currentSong);	
				});
												
			// CHECKING NO. OF COLUMNS + INTRO HIDE
			
				$(".info_bottom").each( function(){
					if ( $(this).find(".col_3").text().length == 0 ) {
						$(this).find(".col_1, .col_2").css({
							"width": "48%",
							"display": "inline-block"
						});
						$(this).find(".col_3").hide();
					}
				});	
				
				$(".lyrics").each( function(){
					if ( $(this).find(".col_4").text().trim().length > 1 ) {
						$(this).find(".col_1, .col_2, .col_3, .col_4").css({
							"width": "23%",
							"display": "inline-block"
						});
					}
				});
				
				$(".intros").each( function(){					
					if ( $(this).text().trim().length == 0 ) {
						$(this).hide();
						$(this).parent().prev(".info_top").hide();
					}					
				});
				
			// INFO BREAKS
			
				$(".intros br").replaceWith("<br><br>");


			// DOWNLOAD ICON
			
			$(".icon").hover( function(){
				$(this).find(".icon_text").show();
			}, function(){
				$(this).find(".icon_text").hide();
			});	
				
				
        	});
        </script>
    </head>
    <body>
		<div id="wrapper">
		
			<div class="menu_show_wrapper">
				<div class="menu_show">
					<a href="" class="menu_toggle">
						<span>
							<img class="no_hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/left_arrow.png" >
							<img class="hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/left_arrow_red.png" >
						</span> <p>Liste des chansons</p>
					</a>
				</div>
			</div>
				
			
			<div id="logo">
				<a href="http://www.gongle.fr">
					<img class="no_hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo2.png">
					<img class="hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo2_red.png">
				</a>
				<div class="clear"></div>
			</div>
			
			<div id="title"><h1>Chants des Négociations</h1></div>

	
			<div id="menu">
				<div class="menu_hide">
					<a href="" class="menu_toggle plus">
						<span>
							<img class="no_hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/right_arrow.png" >
							<img class="hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/right_arrow_red.png" >
						</span>
					</a>
				</div>
				<ul>
				<?php $args = array(
					'post_type'		=> 'chants',
					'order'			=> 'ASC'
				); ?>
				<?php $the_query = new WP_Query( $args ); ?>
				<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li id="<?php the_id(); ?>">
						<a href="">
							<span class="menu_title"><?php the_title(); ?></span>
							<span class="menu_duration"><?php echo get_post_meta($post->ID, "duree", true); ?></span>
						</a>	
					</li>  
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				</ul>
			</div>
			
			<div id="songs">
				<ul>
				<?php $args = array(
					'post_type'		=> 'chants',
					'order'			=> 'ASC'
				); ?>
				<?php $the_query = new WP_Query( $args ); ?>
				<?php if ( $the_query->have_posts() ) : ?>
				<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<li id="song-<?php the_id(); ?>">
						<div class="song">
							<div class="song_top">
								<span class="song_title"><?php the_title(); ?></span>
								<?php $track = get_post_meta($post->ID, "piste_sonore", true);
									if( !empty($track) ): ?>
							
								<div class="play_button">
									<img class="no_hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/play_button.png">
									<img class="hover" src="<?php echo get_stylesheet_directory_uri(); ?>/img/play_button_hover.png">		
								</div>
								
								<?php endif; ?>
								
							</div><!-- end of song_top -->
							<div class="song_bottom">
								
								<div class="song_bottom_wrapper">
									<div class="authors">Paroles : <?php gongle_the_field( "auteurs" ); ?></div>
									<div class="dapres">Musique originale : <?php gongle_the_field( "dapres" ); ?></div>
									<div class="lien_projet">En lien avec le projet : <a href="<?php gongle_the_field( "lien_projet" ); ?>" target="_blank"><?php gongle_the_field( "titre_projet" ); ?></a></div>
								</div>
								
								<div class="info_top">
									<div class="info_wrapper top"><span>Introduction</span></div>
								</div>
								<div class="info_bottom closed">
									<div class="info_wrapper intros">
										<div class="col_1"><?php gongle_the_field( "intro" ); ?></div>
										<div class="col_2"><?php gongle_the_field( "intro_trad1" ); ?></div>
										<div class="col_3"><?php gongle_the_field( "intro_trad2" ); ?></div>				
									</div>
								</div>


								<div class="info_top">
									<div class="info_wrapper top"><span>Paroles</span></div>
								</div>
								<div class="info_bottom closed lyrics">
									<div class="info_wrapper">
										<div class="lyrics_orig col_1">
											<span class="lyrics_title"><?php the_title(); ?></span>
											<?php gongle_the_field( "paroles_orig" ); ?>
										</div>
										<div class="col_2 grey">
											<span class="lyrics_title"><?php gongle_the_field( "titre_trad1" ); ?></span>
											<?php gongle_the_field( "paroles_trad1" ); ?>
										</div>	
										<div class="col_3 grey">
											<span class="lyrics_title"><?php gongle_the_field( "titre_trad2" ); ?></span>
											<?php gongle_the_field( "paroles_trad2" ); ?>
										</div>
										<div class="col_4 grey">
											<span class="lyrics_title"><?php gongle_the_field( "titre_trad3" ); ?></span>
											<?php gongle_the_field( "paroles_trad3" ); ?>
										</div>
									</div>
								</div>
											
							</div>							
							<div class="iframe"><span class="hide"><?php the_permalink(); ?></span></div>
						</div><!-- end of .song -->
						
					</li>  
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				</ul>
			</div><!-- end of #songs -->



			
			<div id="player">
				<div class="current_player"></div>
				<div class="player_space"></div>
			</div>


			<!-- playlist icon -->

			<div id="floating_icons">
				<div class="icon draggable">
					<a href="https://soundcloud.com/gonglegongle/sets/chants-des-n-gociations" target="_blank">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon_cd.png">
					</a>
					<div class="icon_text">À télécharger gratuitement</div>
				</div>
			</div>

			<!-- end of playlist icon -->



		</div><!-- end of wrapper -->
    </body>
</html>