<?php get_header(); ?>
<div class="slideshow background_image">
	<?php
	$scode = get_post_meta($post->ID, "Slideshow", true);
	echo do_shortcode($scode);
	?>
</div>

<div id="single_wrapper" class="container-fluid g-lg-0">
	<div class="single_header_block row">
		<div class="single_title offset-lg-1 col-lg-5">
			<h2>
				<?php the_title(); ?>
			</h2>
			<h3>
				<?php echo get_post_meta($post->ID, 'Year', true); ?>
			</h3>

			<?php if (in_category([50,51])): ?>
			<h3 class="badge bg-danger fs-6 rounded-1">
				<?php if (has_category(50)) { echo 'En création'; } elseif (in_category(51)) { echo 'En tournée'; } ?>
			</h3>
			<?php endif; ?>
		</div>
		<?php $lien_meta = get_post_meta($post->ID, "Lien", true); ?>
		<?php $titre_meta = get_post_meta($post->ID, "Titrelien", true); ?>
		<?php if ( ! empty ( $lien_meta ) ) { ?>
			<div class="single_link offset-lg-1 col-lg-5">
				<a href="<?php echo $lien_meta?>" target="_blank"><span><?php echo ($titre_meta ? $titre_meta : 'Voir le blog / See the blog'); ?> →</span></a>
			</div>
		<?php } ?>

	</div>
	<div class="connector_div">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/connector02.png">
	</div>
	<div class="single_info_block row">
		<?php while ( have_posts() ) : the_post(); ?>

		<div class="single_info_text offset-lg-1 col-lg-6">
			<?php the_content(); ?>
		</div>
		<div class="single_diagrams offset-lg-1 col-lg-3">
			<?php the_content(); ?>
		</div<>
		<?php endwhile; ?>
		<div class="clear">
		</div>
	</div>
</div>

<?php get_footer(); ?>
