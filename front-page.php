<?php get_header(); ?>

<div class="background_image">
	<?php echo do_shortcode('[metaslider id="3015"]'); ?>
</div>

<div id="front_page" class="page_wrapper container-fluid g-lg-0">

	<div class="connector_div">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/connector01.png">
	</div>

		<section id="nav" class="sommaire_list page_post">
		<div class="row">
			<div class="offset-lg-2 col-lg-6">
				<h1>Actualités</h1>
			</div>
		</div>
		<ul>
			<?php
				global $post;
				$args = array( 'posts_per_page' => 6, 'category' => 5 );
				$posts = get_posts( $args );
				foreach ($posts as $post) :
			?>
			<li class="row">
				<div class="offset-lg-1 col-lg-6">
					<a href="#post-<?php the_ID(); ?>" target="_self">
						<?php the_title(); ?>
					</a>
				</div>
				<div class="sommaire_dates offset-lg-1 col-lg-2">
					<?php echo get_post_meta($post->ID, 'Date', true); ?>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</section><!-- end of sommaire -->

	<div class="connector_div_2">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/connector_arrow3.png">
	</div>

	<!-- MAIN CONTENT -->

	<?php query_posts("cat=5"); ?>
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

	<section class="page_post row">
		<div class="offset-lg-1 col-lg-6" id="post-<?php the_ID(); ?>">
			<div class="post_title">
				<?php the_title(); ?>
			</div>
			<div class="post_content">
				<?php the_content(); ?>
			</div>
		</div>
		<div class="post_title offset-lg-1 col-lg-3">
			<div>
				<?php $my_post_meta = get_post_meta($post->ID, "Lien blog", true); ?>
				<?php if ( ! empty ( $my_post_meta ) ) { ?>
				<?php echo "<a href=\"<?php echo get_post_meta($post->ID, 'Lien blog', true); ?>\" target=\"_blank\"><div class=\"accueil_lien_blog\"><span>Voir le blog / See the blog →</span></div></a>"?>
				<?php } ?>
			</div>
			<div class="post_info">
				<p class="date"><?php echo get_post_meta($post->ID, 'Date', true); ?></p>
				<p class="address"><?php echo get_post_meta($post->ID, 'Address', true); ?></p>
			</div>
			<div class="top_link">
				<a href="#nav">retour au sommaire / <br><span class="english">back to index</span></a>
			</div>
		</div>
	</section>

	<?php endwhile; ?>
	<?php endif; ?>

</div><!-- end of front-wrapper -->

		<!-- END OF REAL FRONT PAGE -->

<?php include_once "floating_icons.php"; ?>

<?php get_footer(); ?>
