<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<title>
		<?php
			bloginfo( 'name' );
			wp_title( '|', true, 'left' );
			global $page, $paged;
		?>
		</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>">

		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.bundle.min.js"></script>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.smooth-scroll.min.js"></script>
		<script>
			$(document).ready(function () {
				$('a').smoothScroll({
					offset: -200,
					direction: 'top',
					easing: 'swing',
					speed: 1000,
					autoCoefficent: 2,
				});

				var imgHeight = $(".background_image img").height();
				var windowHeight = $(".background_image").height();

				if ( imgHeight < windowHeight ) {
					} else {
						$(".connector_div").css("height", "1OOvh");
					};

				$("#mc_mv_EMAIL").attr( "placeholder","E-Mail" );
				$("#mc_mv_FNAME").attr( "placeholder","Prénom / First Name" );
				$("#mc_mv_LNAME").attr( "placeholder","Nom / Last Name" );
			});
		</script>

		<?php wp_head(); ?>

	</head>

	<body>
		<div id="wrapper">

			<nav class="header_menu navbar navbar-expand-lg w-100 sticky-top py-0">
				<div class="container-fluid d-lg-none py-1">
					<div class="navbar-brand text-start d-lg-none row align-items-center m-0 g-0">
						<h1 class="site-title col" itemprop="name">
							<a href="<?php echo get_home_url(); ?>" rel="home" itemprop="url" style="display: block;height: 32px;">
							<img height="32" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPcAAABACAYAAADGQ+GeAAAABmJLR0QARQAtAHlwHHBtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5wYEFyc2y6FxZAAAD3tJREFUeNrtnXuQHMV9xz/TPT17SAgknU7hoRdCyAhQcChFEpaNiW1RoJJtCAIryJQNFAqxHTsYxy7iMgkKeWAgNhWXhTEIB0omBiyIY5DLODYJz0Q8hJBkyyAkBHoAeguk25tX/ujeu73dmdm53b27Pam/VVO3tzs93dPz+3b/fr/+9W/AwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsIiN5wj5D6HA6cB08GbBuIECA+C/yRwrxUDi8Od3EcDnwVi4F1zHAD2AO8De4FgkNvrAqOBEUC7OUaBOA68DgjHIJ3jEYyGeCRhNJoo7ABE6hWleIsw+gqwwoqDxeFK7jnAU7VLOJ04Yh9C7MNhHxH7CJ1d4OyB8B0I9gI7zODggHsOuOMgDEEchEAg3YmIYDrIA/isg3AniE6IBchhEI9Exu0IRhFFo4njdqLomL7fndOJK3YQuRsIw+cheAHYCuwHFLgXQXCjHja8dQTOU1D8BfCIFQ2Lw4ncLjADGAMcC4wFMQrUWJy4HdcZQxx1EEcjiaIO4tgbsFZKuQshdhHL7QTRDnDeBX8bRLvMILLPaBcHgPfMscdoIbUwHtr+CToXdX8jvAeJui614mFxpNrcjhkE2oxKf5T5O9yozWOBOxDyHaJwJjAM8Azh9gObTdkTTRlprusDnYasBw1xBwLthujfgM6FyMKjhMX5VkQsLBLRttyQWQ2xdv9Yt9v9W/sMLSyScaEh9xeHXMulu820fYJ9jBYW1RgHxAjvgSHY9jOAGKXW2cdoYZE4A4q9OE7n0Gy8t8zM3tfaB2kx1DAAQSyFO6F4NXAy8HpL94byniVy9hAW5/V8Jzfhh5OAzwP/1iItLQCTgQ56/BmdwNvAG2inZDMwFpgIlJYhDwBvAtuHqLyPB05AO34xA/dOYJv5e1jB7f8qik8AV6PX0VuZ3Gfid83GLfxPr2/9cAZSvEYY/Qjt0V82SO1rA67A9eYTh7MIw/bEs4R8B8d9jrD4EHBfHfWcBu4ClDOfIDiLOJYVv8co9TK+sxK6HgZW5TLPlPpVwvcRvr8QWJNS7o9Q6v7qUs5rhF15VzLmQuFPcaOPEfhTk6c4pwvXfQU/fBSiu8wAloVvotTlCXfzJqE/t77HK25GyU83JCG+swq6Lh9IoZyjR0hxY2sPc2qVGcknJ/w6ASHfNb9/fRBadzFSvGnqz3+4hV8DI/tghny7z3VQuIPaqyFTUssruSmj3McSywi5K8fNjAHv3/t8P8I5BHymhop3X0rZg/Wbr4X/7HvfVz5vtXagBfMkXbm3rIWpvUC38ajvmf8XA39YcU6HEcRYB70MGL7R0ANX8tUcGpqDKjxRfx1qPTCqtgykHerWlHIfSqlvXc3BRLrbGyTL+Rnkvj2lrzc2oJjd2zC5VeE3A27JmlnkyVazsI1QnYPjFOmJZrtMf3b/JqHMMJT6PzNY3TIAbfxcww9cz653Zs8a6vHGBSuTcCfluMbsJpF7GELsbvh+tEyMHlrklpsH2ObGxyHCCca3GLeXgn8VcB1xDPAB4zxajvJexO/6x4RCB/H9mSjvGfyur4F4G6Jb+6mB7cCP0puvNuDHyyHYCmIUrvo0QfEjKX6Pq4HvAusTfryC0P9EOvHFXhyl7erIn0EUJc/Qvn8aiCUQ3VCfWuo+TBgc33i3ecuIujK0iMKdUHzRfD4LiosTT4tjD7xboevKQRVTKfYSynw7F/3wtYFvoJDvIsWeFmL2pN6jnrgRGIHjFI3dcqGZwb1UNbbHBp7TT4PPbRkz8dKUMt9NPF/Kt4BLU4RnT3o97k3oEOMSRoB7Q8bsEaE9+fXM3EkaRl9n7qnptrTcAZyeUGZ28vlit4lUHNyZW6nftbSvqvXI7d1tVOtvQ9s94N6AEO9pdeyoFWWdm6VtjDfqW5AxCDTSZ2+nPOwXs61n7xFDksXAeeglyDR8vIajLK3/bskg6efqJzexaVOd5BY3Z1x3ekY/fBHHW2H8GwvQG6iGt4TN7arf0+IIW2oEUnITTmlveoKHWDvOzslxpUX6AXiPNrmFczKEtIlLHeo7GfVkOcgkQuzrg32fn9xS7KXH+/6RPpHb9dYmn+8902QBGsCZW24Gjs95DApKnrwO9LLIbEOec4FZ6PjtgdtcIgsrTectMssX5Sr6dWVnfsao8BkzrPcTU/aKJrbw6xkEaF6su+M9kjJbrOpDH1aQs/BYgzN3XKYOz+wDuYelDzji+iFL7vxLeO/VGJDrbxlwnFFnLtad2XYvSj2X3uGpI+wnBoDeZxpb9HWcws9M3at7q7BiibE7v1WbJN3e9hFNEp5bSV/frXSCzkDK12seWu2sqKbwmxrkynrk96Q8wxdykztbNs5ABw3lJff4jGstShjYVuTqN/jgkPGWV3j383jLXeNUaTezxmTwTsFxPoCMTiUMJlYnboiAzlIQZIwsPE3Ebpz4EFG8TWds4X1DomPBnYwbfgi/62zgcZzCz4iLX0BnTekPvAx8gTD8PoQnIdWv8LsqI4v0PnIZXUfIeuCn6XpJfBnwEE5hOXHxU83QLVK+PUBUlerqGMLwpNrjBWF1UGpawo0ox16AMC3lVn7/g/BWE3U+BvxzdXvVA/j+NQhxgCgakVNO09BVfXY4HT9Hv/WEqrY64qwOOR34E1CTkOIURDSFMJhIFA9P7KsYnVFNiS2gtuKLNyDYAMHv0YkYtqOzoewlLKZUXxoIglJ2tpNxvFuIixcBnwL3HyC4zVyn2VgKbAJWIjiBkNHA7rJ23QZsA+cW4CFQd4H/LXQKqUr8FFV4Ar/4SWNyPNdg26KUb482z6ycWMWc48VOCCvEQfipQ0FtdaUtRcaKue/S8ScBN+N4M4i7FvT6zfenIeWPieO8efvCzKEtuT+m5LiuP0TInbVPRN1F0vKBUi9ph1HhX4EvA/ONyjSqHxt6gVFxSm25lv5bk19AVlSSW/jvin65LOU6x1E7nDIvvpmhep1Yce7cnCbPs9XiYEySapv7v3LY3D9v2ObWzrNycjYSMDOie8Wj+vjraq1B7shZ36xBVcul2ILO3FvrmJZF8HZ0LO8ZpEbmDDiuMQIQmwd3RT/Vc2mFYF8KnFvmLPsScIYRqhjcv0/RM42dLr7WYHvOzxC2iyrOnQDu3/U6pNyZi9wUfpBiC+9PNQ26yd2dzKIylPSH+cktd6LTb0Fp5aGRaDgdaps04Pwi4eyrevVZqv9hkMntqg0cxnBMKKi5WW9tgoA3A5NTZyMhd+nfvPuNUysG91+S+d3tJBreQFsKhmBJm0F+Wdu+Vs/nIzeXZJDozzNquCij3Jf6SO7hZdrAYw2RO22wSnA2JeDihsndEBFTl8I2cgTgaJPTrExdcW8wKkkzMRG4Ul9bfBW8+0zUWpmpUjIZxJLUWTd5tujLw16eIahZySPm5p+5cY19nxZbfV5CmRllO+SSjva6yQ3DcZxD9ZM7JdpMl3k+wzk2Hun9vPGZW2wx6nGtY0p+ctcfHzLU3jhyIkL8FiE3EEWTiMIxpmPuh84ngY3AM+jUxv2Bq4C7QCzBKZxOfOhi4K+A2yu8wA8QdV2iBwnuqbOuqcCGDJt3JaG/EqJNQBu4U3Hl+anx5a56nsD/4xT7/qZ0CfEeJo7WGG3lVOhcmN5kbxl0XZVC7uq9/FLuIgwn0r1yYvwt8FgNzWQ9vn968m/es/hds5P7TG4ljO+GaC1wCBgH3kwcf1FGqu45RqYqyO1/uW4pkmIfYTSWXl78tnuh8/IETXA/kZfzhRlhAP7VQ3TyLqnn3YEmUxNVUFetNeGTlzW9CULuMLHamFE1BuZVzYg9a98d9VdWz/7q1B1DGzPI8ruGr693YhVSasg7c5cGx59Q/w60iU3rM32cnXvmzh+Ft4eqJcOmrHPHDGGc2K1yKvmqdoR4ZlYRfwni+irvtpKvotRLKLkZ5b2iI6sKPzAe1LnkX8c8W6vp3fHkoBMhlOqaVHH+uWU+gvohC/9B45v4/xf4cEYtx/fBe5ykwgdU73+vn9zgJZoW+fdzX9IEkoToSMFC88ntbusXcjeSLKJlCI66vWLZ45JEgRJye4UHOEU4vRUJs/wE/V3hzl6RVDom/YKy8z5sHth2qrKeFO4w5RY3dstZO8QyZ4gtwFdyVnJcXfu6dVThtJrOynS7Ps3xeA459y2n4DxzXh1E8ZaRueEm03GXR8vZX0XuWtrKETBzV9qDKzJu6M/KiFvuW5DoNemZwMKyDKd6JtGEiKrVKPUdM4iMTdDVv1qmbvWOdOtZMmo0dv5sKCytmW5JiH0mZc811LdbbSF4D2aGhjoExmufd69zX2fubBLlTzddAHGdGYBqDVKrTYadD+YYbFt15j40lB1qlQ6StwjDE9Bv8RwNfBTUPFw+SuCfgiq8hF88K+MKJ4O4ECe+iThuM86O/TjybQK5CjofB14AXsnRmiuBu40N+xy+/0NgFa5aRuDPAP4AeKcZdw2cit4FVP5yxAPo96ZtRr+RtVGMNF7d9jICdqGzhL5B3zKgyuRBkZjkiL/eJkM1AnOvfcF4c4wuI1YnOvpxm7mnvDiWnvX5ehAmyMJI9Cu5GsV2Dg+UXvuTuUH/88BfQGEpyns2MTmBcN4ze4E7GmzQyeYFDBU2r/coR8670C1aRbEd+u13l6CcCwjEVuLiL9HLFmvQgRYPVs8hYi+OWkMQrQZ/DfBbYDX6pYPNwiSj9peWgB60omZhyd1cTDQqbGzUoDeBXfaxW1hYWFhYWFhYWFhYWFhYWFjUC7s803yMRK/pjkaHtpb2RYdoj/xu9FrkPttVFpbcrQsFnA/uDFz3XMLiLOK4kKukcN7H8Z4iLP4aeNocFhaW3C2AeQjngV455pTYQqw2EDhbIdiDjhor5fYS4BwDchQyHAfRNMKwJ2WSLDxNWPwsOsLMwsJikDCBnpzYS9CbHOrJvDIS+GT3G1D0DjcLC4vBg7jekHt+0y7Zs7mhYPvXoilSarugHnilDRubmnZJvztH+1G2fy0suQcNQSlZQ/MS1rvdu4wC278WltyDB7+bks1CKEt7ca1abmHJPXiI9jR9lo2DrU3XBiwsuS36TG5DRHde05Ry5cw0nw/Y/rWwGDwMNxlQY5Rab94E+nFgXA612kNHsM0EFvdO7pDn7ZoWFhb9jTE6X3pKAjwlXzXHxu6/aUka9VtMrrVdatFM2Ai1xjESmA5iFrhnophCGE4mCjtwiMGJIXaIcRByF0JsJnI2EUXrIHgZnQVms+1GCwsLC4tc+H/etLAULT1CkQAAAABJRU5ErkJggg=="/>
							</a>
						</h1>
						<div class="col row ms-1" style="height: 24px;min-width: 89px;">
							<div class="col gx-1">
								<a href="https://www.facebook.com/GONGLE-187071628004553/">
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 22 22">
										<path d="M 2,0 C 0.90625,0 0,0.90625 0,2 v 18 c 0,1.09375 0.90625,2 2,2 h 18 c 1.09375,0 2,-0.90625 2,-2 V 2 C 22,0.90625 21.09375,0 20,0 Z m 0,2 h 18 v 18 h -5.1875 v -6.75 h 2.59375 l 0.375,-3 H 14.8125 V 8.3125 c 0,-0.875 0.214844,-1.46875 1.46875,-1.46875 h 1.625 V 4.125 C 17.628906,4.089844 16.667969,4.03125 15.5625,4.03125 c -2.304687,0 -3.875,1.386719 -3.875,3.96875 v 2.25 h -2.625 v 3 h 2.625 V 20 H 2 Z" />
									</svg>
								</a>
							</div>
							<div class="col gx-1">
								<a href="https://www.instagram.com/groupe_gongle/">
									<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-instagram" viewBox="0 0 16 16">
										<path d="M8 0C5.829 0 5.556.01 4.703.048 3.85.088 3.269.222 2.76.42a3.917 3.917 0 0 0-1.417.923A3.927 3.927 0 0 0 .42 2.76C.222 3.268.087 3.85.048 4.7.01 5.555 0 5.827 0 8.001c0 2.172.01 2.444.048 3.297.04.852.174 1.433.372 1.942.205.526.478.972.923 1.417.444.445.89.719 1.416.923.51.198 1.09.333 1.942.372C5.555 15.99 5.827 16 8 16s2.444-.01 3.298-.048c.851-.04 1.434-.174 1.943-.372a3.916 3.916 0 0 0 1.416-.923c.445-.445.718-.891.923-1.417.197-.509.332-1.09.372-1.942C15.99 10.445 16 10.173 16 8s-.01-2.445-.048-3.299c-.04-.851-.175-1.433-.372-1.941a3.926 3.926 0 0 0-.923-1.417A3.911 3.911 0 0 0 13.24.42c-.51-.198-1.092-.333-1.943-.372C10.443.01 10.172 0 7.998 0h.003zm-.717 1.442h.718c2.136 0 2.389.007 3.232.046.78.035 1.204.166 1.486.275.373.145.64.319.92.599.28.28.453.546.598.92.11.281.24.705.275 1.485.039.843.047 1.096.047 3.231s-.008 2.389-.047 3.232c-.035.78-.166 1.203-.275 1.485a2.47 2.47 0 0 1-.599.919c-.28.28-.546.453-.92.598-.28.11-.704.24-1.485.276-.843.038-1.096.047-3.232.047s-2.39-.009-3.233-.047c-.78-.036-1.203-.166-1.485-.276a2.478 2.478 0 0 1-.92-.598 2.48 2.48 0 0 1-.6-.92c-.109-.281-.24-.705-.275-1.485-.038-.843-.046-1.096-.046-3.233 0-2.136.008-2.388.046-3.231.036-.78.166-1.204.276-1.486.145-.373.319-.64.599-.92.28-.28.546-.453.92-.598.282-.11.705-.24 1.485-.276.738-.034 1.024-.044 2.515-.045v.002zm4.988 1.328a.96.96 0 1 0 0 1.92.96.96 0 0 0 0-1.92zm-4.27 1.122a4.109 4.109 0 1 0 0 8.217 4.109 4.109 0 0 0 0-8.217zm0 1.441a2.667 2.667 0 1 1 0 5.334 2.667 2.667 0 0 1 0-5.334z"/>
									</svg>
								</a>
							</div>
							<div class="col gx-1">
								<a href="<?php echo get_home_url(); ?>/newsletter">
									<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" class="bi bi-envelope" height="16" width="20" viewBox="1 1 14 13">
										<path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
									</svg>
								</a>
							</div>
						</div>
					</div>
					<button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
						<svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
							<path d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
						</svg>
					</button>
				</div>
				<div class="collapse navbar-collapse justify-content-between justify-content-lg-center py-4 py-lg-2" id="navbarNav">
					<ul class="navbar-nav justify-content-around">
						<li class="nav-item">
							<a class="nav-link" href="<?php echo get_home_url(); ?>"><span class="d-lg-block mb-2">actualités</span><span class="d-lg-none"> / </span><span class="english d-lg-block">news</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo get_home_url(); ?>/projets"><span class="d-lg-block mb-2">projets</span><span class="d-lg-none"> / </span><span class="english d-lg-block">projects</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo get_home_url(); ?>/le-groupe"><span class="d-lg-block mb-2">le groupe</span><span class="d-lg-none"> / </span><span class="english d-lg-block">the group</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo get_home_url(); ?>/recherches"><span class="d-lg-block mb-2">recherches</span><span class="d-lg-none"> / </span><span class="english d-lg-block">research</span></a>
						</li>
					</ul>
				</div>
			</nav>

			<div class="business_card_wrapper d-none d-lg-block mb-2">

				<div id="bizcard" class="collapse show business_card">
					  <h1 class="site-title">
						<a href="<?php echo get_home_url(); ?>"><img height="32" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPcAAABACAYAAADGQ+GeAAAABmJLR0QARQAtAHlwHHBtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5wYEFyc2y6FxZAAAD3tJREFUeNrtnXuQHMV9xz/TPT17SAgknU7hoRdCyAhQcChFEpaNiW1RoJJtCAIryJQNFAqxHTsYxy7iMgkKeWAgNhWXhTEIB0omBiyIY5DLODYJz0Q8hJBkyyAkBHoAeguk25tX/ujeu73dmdm53b27Pam/VVO3tzs93dPz+3b/fr/+9W/AwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsIiN5wj5D6HA6cB08GbBuIECA+C/yRwrxUDi8Od3EcDnwVi4F1zHAD2AO8De4FgkNvrAqOBEUC7OUaBOA68DgjHIJ3jEYyGeCRhNJoo7ABE6hWleIsw+gqwwoqDxeFK7jnAU7VLOJ04Yh9C7MNhHxH7CJ1d4OyB8B0I9gI7zODggHsOuOMgDEEchEAg3YmIYDrIA/isg3AniE6IBchhEI9Exu0IRhFFo4njdqLomL7fndOJK3YQuRsIw+cheAHYCuwHFLgXQXCjHja8dQTOU1D8BfCIFQ2Lw4ncLjADGAMcC4wFMQrUWJy4HdcZQxx1EEcjiaIO4tgbsFZKuQshdhHL7QTRDnDeBX8bRLvMILLPaBcHgPfMscdoIbUwHtr+CToXdX8jvAeJui614mFxpNrcjhkE2oxKf5T5O9yozWOBOxDyHaJwJjAM8Azh9gObTdkTTRlprusDnYasBw1xBwLthujfgM6FyMKjhMX5VkQsLBLRttyQWQ2xdv9Yt9v9W/sMLSyScaEh9xeHXMulu820fYJ9jBYW1RgHxAjvgSHY9jOAGKXW2cdoYZE4A4q9OE7n0Gy8t8zM3tfaB2kx1DAAQSyFO6F4NXAy8HpL94byniVy9hAW5/V8Jzfhh5OAzwP/1iItLQCTgQ56/BmdwNvAG2inZDMwFpgIlJYhDwBvAtuHqLyPB05AO34xA/dOYJv5e1jB7f8qik8AV6PX0VuZ3Gfid83GLfxPr2/9cAZSvEYY/Qjt0V82SO1rA67A9eYTh7MIw/bEs4R8B8d9jrD4EHBfHfWcBu4ClDOfIDiLOJYVv8co9TK+sxK6HgZW5TLPlPpVwvcRvr8QWJNS7o9Q6v7qUs5rhF15VzLmQuFPcaOPEfhTk6c4pwvXfQU/fBSiu8wAloVvotTlCXfzJqE/t77HK25GyU83JCG+swq6Lh9IoZyjR0hxY2sPc2qVGcknJ/w6ASHfNb9/fRBadzFSvGnqz3+4hV8DI/tghny7z3VQuIPaqyFTUssruSmj3McSywi5K8fNjAHv3/t8P8I5BHymhop3X0rZg/Wbr4X/7HvfVz5vtXagBfMkXbm3rIWpvUC38ajvmf8XA39YcU6HEcRYB70MGL7R0ANX8tUcGpqDKjxRfx1qPTCqtgykHerWlHIfSqlvXc3BRLrbGyTL+Rnkvj2lrzc2oJjd2zC5VeE3A27JmlnkyVazsI1QnYPjFOmJZrtMf3b/JqHMMJT6PzNY3TIAbfxcww9cz653Zs8a6vHGBSuTcCfluMbsJpF7GELsbvh+tEyMHlrklpsH2ObGxyHCCca3GLeXgn8VcB1xDPAB4zxajvJexO/6x4RCB/H9mSjvGfyur4F4G6Jb+6mB7cCP0puvNuDHyyHYCmIUrvo0QfEjKX6Pq4HvAusTfryC0P9EOvHFXhyl7erIn0EUJc/Qvn8aiCUQ3VCfWuo+TBgc33i3ecuIujK0iMKdUHzRfD4LiosTT4tjD7xboevKQRVTKfYSynw7F/3wtYFvoJDvIsWeFmL2pN6jnrgRGIHjFI3dcqGZwb1UNbbHBp7TT4PPbRkz8dKUMt9NPF/Kt4BLU4RnT3o97k3oEOMSRoB7Q8bsEaE9+fXM3EkaRl9n7qnptrTcAZyeUGZ28vlit4lUHNyZW6nftbSvqvXI7d1tVOtvQ9s94N6AEO9pdeyoFWWdm6VtjDfqW5AxCDTSZ2+nPOwXs61n7xFDksXAeeglyDR8vIajLK3/bskg6efqJzexaVOd5BY3Z1x3ekY/fBHHW2H8GwvQG6iGt4TN7arf0+IIW2oEUnITTmlveoKHWDvOzslxpUX6AXiPNrmFczKEtIlLHeo7GfVkOcgkQuzrg32fn9xS7KXH+/6RPpHb9dYmn+8902QBGsCZW24Gjs95DApKnrwO9LLIbEOec4FZ6PjtgdtcIgsrTectMssX5Sr6dWVnfsao8BkzrPcTU/aKJrbw6xkEaF6su+M9kjJbrOpDH1aQs/BYgzN3XKYOz+wDuYelDzji+iFL7vxLeO/VGJDrbxlwnFFnLtad2XYvSj2X3uGpI+wnBoDeZxpb9HWcws9M3at7q7BiibE7v1WbJN3e9hFNEp5bSV/frXSCzkDK12seWu2sqKbwmxrkynrk96Q8wxdykztbNs5ABw3lJff4jGstShjYVuTqN/jgkPGWV3j383jLXeNUaTezxmTwTsFxPoCMTiUMJlYnboiAzlIQZIwsPE3Ebpz4EFG8TWds4X1DomPBnYwbfgi/62zgcZzCz4iLX0BnTekPvAx8gTD8PoQnIdWv8LsqI4v0PnIZXUfIeuCn6XpJfBnwEE5hOXHxU83QLVK+PUBUlerqGMLwpNrjBWF1UGpawo0ox16AMC3lVn7/g/BWE3U+BvxzdXvVA/j+NQhxgCgakVNO09BVfXY4HT9Hv/WEqrY64qwOOR34E1CTkOIURDSFMJhIFA9P7KsYnVFNiS2gtuKLNyDYAMHv0YkYtqOzoewlLKZUXxoIglJ2tpNxvFuIixcBnwL3HyC4zVyn2VgKbAJWIjiBkNHA7rJ23QZsA+cW4CFQd4H/LXQKqUr8FFV4Ar/4SWNyPNdg26KUb482z6ycWMWc48VOCCvEQfipQ0FtdaUtRcaKue/S8ScBN+N4M4i7FvT6zfenIeWPieO8efvCzKEtuT+m5LiuP0TInbVPRN1F0vKBUi9ph1HhX4EvA/ONyjSqHxt6gVFxSm25lv5bk19AVlSSW/jvin65LOU6x1E7nDIvvpmhep1Yce7cnCbPs9XiYEySapv7v3LY3D9v2ObWzrNycjYSMDOie8Wj+vjraq1B7shZ36xBVcul2ILO3FvrmJZF8HZ0LO8ZpEbmDDiuMQIQmwd3RT/Vc2mFYF8KnFvmLPsScIYRqhjcv0/RM42dLr7WYHvOzxC2iyrOnQDu3/U6pNyZi9wUfpBiC+9PNQ26yd2dzKIylPSH+cktd6LTb0Fp5aGRaDgdaps04Pwi4eyrevVZqv9hkMntqg0cxnBMKKi5WW9tgoA3A5NTZyMhd+nfvPuNUysG91+S+d3tJBreQFsKhmBJm0F+Wdu+Vs/nIzeXZJDozzNquCij3Jf6SO7hZdrAYw2RO22wSnA2JeDihsndEBFTl8I2cgTgaJPTrExdcW8wKkkzMRG4Ul9bfBW8+0zUWpmpUjIZxJLUWTd5tujLw16eIahZySPm5p+5cY19nxZbfV5CmRllO+SSjva6yQ3DcZxD9ZM7JdpMl3k+wzk2Hun9vPGZW2wx6nGtY0p+ctcfHzLU3jhyIkL8FiE3EEWTiMIxpmPuh84ngY3AM+jUxv2Bq4C7QCzBKZxOfOhi4K+A2yu8wA8QdV2iBwnuqbOuqcCGDJt3JaG/EqJNQBu4U3Hl+anx5a56nsD/4xT7/qZ0CfEeJo7WGG3lVOhcmN5kbxl0XZVC7uq9/FLuIgwn0r1yYvwt8FgNzWQ9vn968m/es/hds5P7TG4ljO+GaC1wCBgH3kwcf1FGqu45RqYqyO1/uW4pkmIfYTSWXl78tnuh8/IETXA/kZfzhRlhAP7VQ3TyLqnn3YEmUxNVUFetNeGTlzW9CULuMLHamFE1BuZVzYg9a98d9VdWz/7q1B1DGzPI8ruGr693YhVSasg7c5cGx59Q/w60iU3rM32cnXvmzh+Ft4eqJcOmrHPHDGGc2K1yKvmqdoR4ZlYRfwni+irvtpKvotRLKLkZ5b2iI6sKPzAe1LnkX8c8W6vp3fHkoBMhlOqaVHH+uWU+gvohC/9B45v4/xf4cEYtx/fBe5ykwgdU73+vn9zgJZoW+fdzX9IEkoToSMFC88ntbusXcjeSLKJlCI66vWLZ45JEgRJye4UHOEU4vRUJs/wE/V3hzl6RVDom/YKy8z5sHth2qrKeFO4w5RY3dstZO8QyZ4gtwFdyVnJcXfu6dVThtJrOynS7Ps3xeA459y2n4DxzXh1E8ZaRueEm03GXR8vZX0XuWtrKETBzV9qDKzJu6M/KiFvuW5DoNemZwMKyDKd6JtGEiKrVKPUdM4iMTdDVv1qmbvWOdOtZMmo0dv5sKCytmW5JiH0mZc811LdbbSF4D2aGhjoExmufd69zX2fubBLlTzddAHGdGYBqDVKrTYadD+YYbFt15j40lB1qlQ6StwjDE9Bv8RwNfBTUPFw+SuCfgiq8hF88K+MKJ4O4ECe+iThuM86O/TjybQK5CjofB14AXsnRmiuBu40N+xy+/0NgFa5aRuDPAP4AeKcZdw2cit4FVP5yxAPo96ZtRr+RtVGMNF7d9jICdqGzhL5B3zKgyuRBkZjkiL/eJkM1AnOvfcF4c4wuI1YnOvpxm7mnvDiWnvX5ehAmyMJI9Cu5GsV2Dg+UXvuTuUH/88BfQGEpyns2MTmBcN4ze4E7GmzQyeYFDBU2r/coR8670C1aRbEd+u13l6CcCwjEVuLiL9HLFmvQgRYPVs8hYi+OWkMQrQZ/DfBbYDX6pYPNwiSj9peWgB60omZhyd1cTDQqbGzUoDeBXfaxW1hYWFhYWFhYWFhYWFhYWFjUC7s803yMRK/pjkaHtpb2RYdoj/xu9FrkPttVFpbcrQsFnA/uDFz3XMLiLOK4kKukcN7H8Z4iLP4aeNocFhaW3C2AeQjngV455pTYQqw2EDhbIdiDjhor5fYS4BwDchQyHAfRNMKwJ2WSLDxNWPwsOsLMwsJikDCBnpzYS9CbHOrJvDIS+GT3G1D0DjcLC4vBg7jekHt+0y7Zs7mhYPvXoilSarugHnilDRubmnZJvztH+1G2fy0suQcNQSlZQ/MS1rvdu4wC278WltyDB7+bks1CKEt7ca1abmHJPXiI9jR9lo2DrU3XBiwsuS36TG5DRHde05Ry5cw0nw/Y/rWwGDwMNxlQY5Rab94E+nFgXA612kNHsM0EFvdO7pDn7ZoWFhb9jTE6X3pKAjwlXzXHxu6/aUka9VtMrrVdatFM2Ai1xjESmA5iFrhnophCGE4mCjtwiMGJIXaIcRByF0JsJnI2EUXrIHgZnQVms+1GCwsLC4tc+H/etLAULT1CkQAAAABJRU5ErkJggg=="/></a>
						</h1>
						<div class="site-description">
							<p><em>Expérimentations sociales et théâtrales</em></p>
							<p class="english" style="color: #302d31;"><em>Social and theatrical experimentation</em></p>
						</div>
						<div class="business_card_address">
							<p>22 rue Raspail</p>
							<p>93100 Montreuil, France</p>
							<p><span class="email_1">contact</span><span class="email_2">gongle</span>fr</p>
							<p class="network_wrapper">
								<a class="facebook_div" href="https://www.facebook.com/GONGLE-187071628004553/"></a>
								<a class="instagram_div" href="https://www.instagram.com/groupe_gongle/"></a>
							</p>
						</div>
						<p><a href="<?php echo get_home_url(); ?>/newsletter">Newsletter - s'inscrire / sign up</a></li>
				</div>
				<button style="position: absolute; bottom: 0; right: 0; background-color: var(--red-color);" class="btn" data-bs-toggle="collapse" data-bs-target="#bizcard" role="button" aria-expanded="true" aria-controls="bizcard">
					<i class="bi"></i>
				</button>
			</div>
