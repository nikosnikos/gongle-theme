<div id="floating_icons">
	<?php $the_query = new WP_Query( 'post_type=icons' ); ?>
	<?php if ( $the_query->have_posts() ) : ?>
	<?php
		// Ensemble de combinaisons de positions pour les icones.
		// A choisir ensuite de manière aléatoire.
		$icons_positions = [
			[
				['top' => 30, 'left' => 360],
				['top' => 300, 'left' => 80],
				['top' => 330, 'left' => 230],
				['top' => 330, 'left' => 900],
			],
			[
				['top' => 160, 'left' => 100],
				['top' => 310, 'left' => 420],
				['top' => 150, 'left' => 860],
				['top' => 320, 'left' => 970],
			],
			[
				['top' => 390, 'left' => 130],
				['top' => 180, 'left' => 240],
				['top' => 10, 'left' => 460],
				['top' => 0, 'left' => 610],
			],
			[
				['top' => 20, 'left' => 180],
				['top' => 50, 'left' => 470],
				['top' => 270, 'left' => 950],
				['top' => 400, 'left' => 110],
			],
			[
				['top' => 390, 'left' => 410],
				['top' => 60, 'left' => 30],
				['top' => 170, 'left' => 230],
				['top' => 90, 'left' => 770],
			],
			[
				['top' => 140, 'left' => 520],
				['top' => 140, 'left' => 190],
				['top' => 390, 'left' => 710],
				['top' => 50, 'left' => 700],
			],
			[
				['top' => 390, 'left' => 280],
				['top' => 190, 'left' => 640],
				['top' => 280, 'left' => 990],
				['top' => 80, 'left' => 480],
			],
		];
		// Prend un des ensemble de positions pour les icones
		$rand_positions = $icons_positions[array_rand($icons_positions)];
		// Et mélange les positions (pour ne pas que ce soit toujours la radio -par exemple- qui soit à la même position dans cet ensemble)
		shuffle($rand_positions);
	?>
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php $positions = array_pop($rand_positions);?>
			<div class="icon" style="top: <?php echo $positions['top']; ?>px; left:  <?php echo $positions['left']; ?>px;">
				<?php $image = get_the_post_thumbnail($post);
					if( !empty($image) ): ?>
						<a href="<?php gongle_the_field('lien'); ?>" target="_blank">
						<?php echo $image; ?>
						</a>
					<?php endif; ?>
				<div class="icon_text"><?php the_title(); ?></div>
			</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
</div>
